#include "Car.h"

#define RIGHT_SENSOR_PIN  10
#define LEFT_SENSOR_PIN   9

void setup() {
  Serial.begin(9600);
  
  init_motor(); 
}

void loop() {
  bool ls = !digitalRead(LEFT_SENSOR_PIN);
  bool rs = !digitalRead(RIGHT_SENSOR_PIN);

  if (ls && rs) {
    car.back(255);
  }
  else if (rs) {
    car.turn(Direction::Right, 255, 1023);
  }
  else if (ls) {
    car.turn(Direction::Left, 255, 1023);
  }
  else {
    car.stop();
  }
}
