#pragma once

#include "Direction.h"

static uint16_t MAX_CAR_SPEED = 1023;

const int LWS_PIN = 5;  // left wheel speed
const int LWR_PIN = 4;  // left wheel rotation

const int RWS_PIN = 6;  // right wheel speed
const int RWR_PIN = 7;  // right wheel rotation


class Car {
  void set_wheels_mode(Direction);
  
public:
  void forward(uint16_t car_speed = 0);
  void back(uint16_t car_speed = 0);

  void turn(Direction, uint16_t lws, int rws); // lws - left wheel speed, rws - right wheel speed
  void rotate(Direction, uint16_t rotation_speed = MAX_CAR_SPEED);

  void set_speed(uint16_t car_speed = 0);

  void stop();
};

void Car::set_wheels_mode(Direction dir) {
  switch (dir) {
    case Direction::Left:
      digitalWrite(LWR_PIN, LOW);
      digitalWrite(RWR_PIN, HIGH);
      break;
    case Direction::Right:
      digitalWrite(LWR_PIN, HIGH);
      digitalWrite(RWR_PIN, LOW);
      break;
    case Direction::Forward:
      digitalWrite(LWR_PIN, HIGH);
      digitalWrite(RWR_PIN, HIGH);
      break;
    case Direction::Backward:
      digitalWrite(LWR_PIN, LOW);
      digitalWrite(RWR_PIN, LOW);
      break;
    default:
      return;
  }
}

void Car::forward(uint16_t car_speed) {
  set_wheels_mode(Direction::Forward);
  set_speed(car_speed);
}

void Car::back(uint16_t car_speed) {
  set_wheels_mode(Direction::Backward);
  set_speed(car_speed);
}

void Car::set_speed(uint16_t car_speed) {
  analogWrite(LWS_PIN, car_speed);
  analogWrite(RWS_PIN, car_speed);
}

void Car::stop() {
  set_speed(0);
}

void Car::rotate(Direction dir, uint16_t rotation_speed) {
  if (dir == Direction::Forward || dir == Direction::Backward)
    return;
   
  set_wheels_mode(dir);
  set_speed(rotation_speed);
}

void Car::turn(Direction dir, uint16_t lws, int rws) {
  set_wheels_mode(dir);

  analogWrite(LWS_PIN, lws);
  analogWrite(RWS_PIN, rws);
}


static Car car;

void init_motor() {
  pinMode(LWR_PIN, OUTPUT);
  pinMode(RWR_PIN, OUTPUT);
  pinMode(RWS_PIN, OUTPUT);
  pinMode(LWS_PIN, OUTPUT);

  car.stop();
}
