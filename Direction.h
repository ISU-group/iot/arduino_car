#pragma once

enum class Direction : uint8_t {
  Left = 0,
  Right,
  Forward,
  Backward
};
